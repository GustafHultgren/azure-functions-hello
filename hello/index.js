module.exports = function (context, req) {
    console.log(context.res);
    context.res = {
        status: 200,
        body: 'hello world!',
        headers: {
            'Content-Type': 'application/json'
        }
    };
    context.done();
};